/* Declararea variabilelor | var, let, const */

var   x      // variabila globala
let   y      // variabila dependenta de bloc
const z = 0; // constanta 
// Nota: tipul `const` necesita valoare



/* Tipuri de date | number, string, object, boolean, array */

let   string  = 'Javascript';
const number  = 13;
const boolean = true;
const object  = { 
    firstName:  "Redko",
    'lastName': "Denis"
};
object.lastName = 'Andrei';
// Nota: variabilele `const` de tip `object` pot fi modificate
const array = [number, string, object, , boolean];
// Nota: `array`-urile pot contine orice tipuri de date, chiar si combinate



/* Afișarea în consolă, alipirea a doua string-uri*/

console.log('Acesta este limbajul ' + string); // concatinarea prin `+`
console.log(`Numarul ${number}`);              // concatinarea 

alert(object.lastName);


/****************************** 
    +	    Adunare             
    -	    Scădere 
    *	    Înmulțire   
    /	    Împărțire   
    %	    Restul împărțirii   
    ++	    Incrementare    
    --	    Decrementare    
******************************/

/* Cicluri repetitive */

var text = "";
let i = 0;
for (let i = 0; i < 5; i++) {
    text += i + ' ';
}
console.log('for', text, i);
// Nota: `let i` este valabila doar in blocul `for`

var text = "";
var j = 0;
while (j < 5) {
    text += j + " ";
    j++;
}
console.log('while', text, j);
// Nota: `var j` a fost modificata la nivel global


/* Functii */

// Funcție obisnuita
function adunare(n1, n2) {
    return n1 + n2;
}
console.log(adunare(6,12));

// Funcțiile pot fi atribuite variabilelor
const inmultire = function (n1, n2) {
    return n1 * n2;
}
console.log(inmultire(3,4));

// Funcție săgeată (arrow function)
const scadere = (n1, n2) => {
    return n1 - n2;
}
console.log(scadere(32,22));

// Utilizarea functiilor predefinite (ex: forEach)
array.forEach((value, index) => {
    console.log(`array[${index}] = `, value);
})
